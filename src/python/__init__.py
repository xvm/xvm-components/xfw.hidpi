"""
SPDX-License-Identifier: MIT
Copyright (c) 2022 XVM Contributors
"""

#
# Imports
#

# xfw.native
from xfw_native.python import XFWNativeModuleWrapper



#
# XFW Loader
#

__xfw_hidpi_initialized = False

def xfw_is_module_loaded():
    global __xfw_hidpi_initialized
    return __xfw_hidpi_initialized

def xfw_module_init():
    hidpi_native = XFWNativeModuleWrapper('com.modxvm.xfw.hidpi', 'xfw_hidpi.pyd', 'XFW_HiDPI')
    hidpi_native.fix_dpi()
    hidpi_native.fix_registry()

    global __xfw_hidpi_initialized
    __xfw_hidpi_initialized = True
