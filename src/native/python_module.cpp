// SPDX-License-Identifier: MIT
// Copyright (c) 2022 XVM Contributors

//
// Imports
//

// stdlib
#include <cstdlib>

// Windows
#include <windows.h>

// pybind11
#include <pybind11/pybind11.h>



//
// Helper
//

bool fix_dpi(){
    return SetProcessDPIAware() != 0;
}


bool fix_registry(){
    bool result{};

    HKEY key = NULL;
    if (RegCreateKeyExW(HKEY_CURRENT_USER, L"Software\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers",
        0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &key, NULL) == ERROR_SUCCESS)
    {
        wchar_t* exe_path{};
        if(_get_wpgmptr(&exe_path) == 0){
            RegSetValueExW(key, exe_path, 0, REG_SZ, (LPBYTE)L"~ HIGHDPIAWARE", (lstrlenW(L"~ HIGHDPIAWARE") + 1) * sizeof(wchar_t));
            result = true;
        }
        RegCloseKey(key);
    }

    return result;
}



//
// Module
//

PYBIND11_MODULE(XFW_HiDPI, m) {
    m.doc() = "XFW HiDPI module";
    m.def("fix_dpi", &fix_dpi);
    m.def("fix_registry", &fix_registry);
}
